/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chang.assignment2Bean;

import java.sql.Timestamp;
import java.util.Objects;
import javafx.beans.property.*;

/**
 *this class is for the table Surgical
 * @author cha_xi
 */
public class SurgicalBean {
    private IntegerProperty id;
    private IntegerProperty patientId;
    private ObjectProperty<Timestamp> dateOfSurgery;
    private StringProperty surgery;
    private DoubleProperty roomFee;
    private DoubleProperty surgeonFee;
    private DoubleProperty supplies;

    public SurgicalBean(int id, int patientId, Timestamp dateOfSurgery, String surgery, double roomFee, double surgeonFee, double supplies) {
        this.id = new SimpleIntegerProperty(id);
        this.patientId = new SimpleIntegerProperty(patientId);
        this.dateOfSurgery = new SimpleObjectProperty<Timestamp>(dateOfSurgery);
        this.surgery = new SimpleStringProperty(surgery);
        this.roomFee = new SimpleDoubleProperty(roomFee);
        this.surgeonFee = new SimpleDoubleProperty(surgeonFee);
        this.supplies = new SimpleDoubleProperty(supplies);
    }
    
    
    public SurgicalBean() {
       this.id = new SimpleIntegerProperty();
        this.patientId = new SimpleIntegerProperty();
        this.dateOfSurgery = new SimpleObjectProperty<Timestamp>();
        this.surgery = new SimpleStringProperty();
        this.roomFee = new SimpleDoubleProperty();
        this.surgeonFee = new SimpleDoubleProperty();
        this.supplies = new SimpleDoubleProperty();        
    }

    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public int getPatientId() {
        return patientId.get();
    }

    public void setPatientId(int patientId) {
        this.patientId.set(patientId);
    }

    public Timestamp getDateOfSurgery() {
        return dateOfSurgery.get();
    }

    public void setDateOfSurgery(Timestamp dateOfSurgery) {
        this.dateOfSurgery.set(dateOfSurgery);
    }

    public String getSurgery() {
        return surgery.get();
    }

    public void setSurgery(String surgery) {
        this.surgery.set(surgery);
    }

    public double getRoomFee() {
        return roomFee.get();
    }

    public void setRoomFee(double roomFee) {
        this.roomFee.set(roomFee);
    }

    public double getSurgeonFee() {
        return surgeonFee.get();
    }

    public void setSurgeonFee(double surgeonFee) {
        this.surgeonFee.set(surgeonFee);
    }

    public double getSupplies() {
        return supplies.get();
    }

    public void setSupplies(double supplies) {
        this.supplies.set(supplies);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.id.get();
        hash = 29 * hash + this.patientId.get();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SurgicalBean other = (SurgicalBean) obj;
        if (this.id.get() != other.id.get()) {
            return false;
        }
        if (this.patientId.get() != other.patientId.get()) {
            return false;
        }
        if (!Objects.equals(this.dateOfSurgery.get(), other.dateOfSurgery.get())) {
            return false;
        }
        if (!Objects.equals(this.surgery.get(), other.surgery.get())) {
            return false;
        }
        if (this.roomFee.get() != other.roomFee.get()) {
            return false;
        }
        if (this.surgeonFee.get() != other.surgeonFee.get()) {
            return false;
        }
        if (this.supplies.get() != other.supplies.get()) {
            return false;
        }
        return true;
    }

   

    @Override
    public String toString() {
        return "SurgicalBean{" + "id=" + id.get() + ", patientId=" + patientId.get()
                + ", dateOfSurgery=" + dateOfSurgery.get().toString() + ", surgery=" + surgery.get()
                + ", roomFee=" + roomFee.get() + ", surgeonFee=" + surgeonFee.get()
                + ", supplies=" + supplies.get() + '}';
    }
    
    
}
