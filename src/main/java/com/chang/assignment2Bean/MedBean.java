/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chang.assignment2Bean;

import java.sql.Timestamp;
import java.util.Objects;
import javafx.beans.property.*;

/**
 *
 * @author cha_xi
 */
public class MedBean {

    private IntegerProperty ID;
    private IntegerProperty patientID;
    private ObjectProperty<Timestamp> dateOfMed;
    private StringProperty med;
    private DoubleProperty unitCost;
    private DoubleProperty units;

    public MedBean(int ID, int patientID, Timestamp dateOfMed, String med, double unitCost, double units) {
        this.ID = new SimpleIntegerProperty(ID);
        this.patientID = new SimpleIntegerProperty(patientID);
        this.dateOfMed = new SimpleObjectProperty<Timestamp>(dateOfMed);
        this.med = new SimpleStringProperty(med);
        this.unitCost = new SimpleDoubleProperty(unitCost);
        this.units = new SimpleDoubleProperty(units);
    }

    public MedBean() {
        this.ID = new SimpleIntegerProperty();
        this.patientID = new SimpleIntegerProperty();
        this.dateOfMed = new SimpleObjectProperty<Timestamp>();
        this.med = new SimpleStringProperty();
        this.unitCost = new SimpleDoubleProperty();
        this.units = new SimpleDoubleProperty();
    }
    

    public int getID() {
        return ID.get();
    }

    public void setID(int ID) {
        this.ID.set(ID);
    }

    public int getPatientID() {
        return patientID.get();
    }

    public void setPatientID(int patientID) {
        this.patientID.set(patientID);
    }

    public Timestamp getDateOfMed() {
        return dateOfMed.get();
    }

    public void setDateOfMed(Timestamp dateOfMed) {
        this.dateOfMed.set(dateOfMed);
    }

    public String getMed() {
        return med.get();
    }

    public void setMed(String med) {
        this.med.set(med);
    }

    public double getUnitCost() {
        return unitCost.get();
    }

    public void setUnitCost(double unitCost) {
        this.unitCost.set(unitCost);
    }

    public double getUnits() {
        return units.get();
    }

    public void setUnits(double units) {
        this.units.set(units);
    }

    @Override
    public String toString() {
        return "MedBean{" + "ID=" + ID.get() + ", patientID=" + patientID.get() 
                + ", dateOfMed=" + dateOfMed.get().toString() + ", med=" + med.get()
                + ", unitCost=" + unitCost.get() + ", units=" + units.get() + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.ID.get();
        hash = 67 * hash + this.patientID.get();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MedBean other = (MedBean) obj;
        if (this.ID.get() != other.ID.get()) {
            return false;
        }
        if (this.patientID.get() != other.patientID.get()) {
            return false;
        }
        if (!Objects.equals(this.dateOfMed.get(), other.dateOfMed.get())) {
            return false;
        }
        if (!Objects.equals(this.med.get(), other.med.get())) {
            return false;
        }
        if (Double.doubleToLongBits(this.unitCost.get()) != Double.doubleToLongBits(other.unitCost.get())) {
            return false;
        }
        if (Double.doubleToLongBits(this.units.get()) != Double.doubleToLongBits(other.units.get())) {
            return false;
        }
        return true;
    }

}
